fetch('http://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(json));

// Getting all TO DO LIST ITEM

fetch('http://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => {

	let list = json.map((todo => {
		return todo.title;
	}))

console.log(list);

})

// Getting a specific to do list item

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(`The item "${json.title}" on the list has a status of ${json.completed}`));



// Create a to do list item using POST method
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: "POST",
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		"title": "Created To Do List Item",
		"completed": false,
		"userId": 1
	})
}).then((response) => response.json())
.then((json) => console.log(json));

// Update a to do list item using PUT method
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: "PUT",
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: "Updated To Do List Item",
		description: "To update the my to do list with a different data structure.",
		status: "Pending",
		dateCompleted: "Pending",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// Updating a to do list item using PATCH method
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: "PATCH",
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		status: "Complete",
		dateCompleted: "07/09/21",
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// Delete a to do list item
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: "PATCH"
})

