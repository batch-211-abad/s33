/* fetch('https://jsonplaceholder.typicode.com/posts/1')
.then((response) => response.json())
.then((json) => console.log(json));

/*
	Postman

	url: https://jsonplaceholder.typicode.com/posts/1
	method: GET
	

*/


/*


	CREATING A POST
	Syntax:
		.fetch('URL', options)
		.then((response) => {})
		.then((response) => {})
	

*/


/*
// Creates a new post following the REST API (create, /post/:id, POST)
	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			title: 'New post',
			body: 'Hello World',
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json))

*/
/*

	POSTMAN
	url: https://jsonplaceholder.typicode.com/posts
	method: POST
	body: raw + JSON
	{
		"title": "My First Blog Post",
		"body": "Hello World!",
		"userId": 1
	}

// UPDATING A POST

// Updates a specific post following the REST API (update, /posts/:id, PUT)

*/
/*
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		id: 1,
		title: "Updated post",
		body: "Hello Again!",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

*/
/* 
	
	POSTMANL

	url: https://jsonplaceholder.typicode.com/posts/1
	method: PUT
	body: raw + json
	{
		"title": "My First Revised Blog Post",
		"body": "Hello there! I revised this a bit.",
		"userId": 1
	}

*/

// Update a post using the PATCH method
/*
	- Updates a specific post following the Rest API (update, /posts/:id, Patch)
	- The differences between PUT and PATCH is the number of properties being changed.
	- PATCH is used to update the whole project
	- PUT is used to update a single/several properties

*/
/*
	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'PATCH',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			title: "Corrected post"
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json));

*/
/* 
	POSTMAN
		url: https://jsonplaceholder.typicode.com/posts/1
		method: PATCH
		body: raw + json
			{
				"title": "This is my final title."
			}

*/

// DELETING A POST
// Deleting a specific post following the REST API (delete, /post/:id, DELETE)
/*	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'DELETE'
	})


*/

// FILTERING THE POST
/*
	- The data can be filtered by sending the userID along with the URL
	- Information sent via the URL can be done by adding the question mark symbol ( ? )
	- Syntax:
		Individual Parameters:
			'url?parameterName=value'
		Multiple Parameters:
		'url?ParamA=valueA&paramB=valueB'

*/
/*
	fetch('https://jsonplaceholder.typicode.com/posts?userId=1&userId2&userId=3')
	.then((response) => response.json())
	.then((json) => console.log(json));
*/

// RETRIEVE COMMENTS OF A SPECIFIC POST

// Retrieving comments for a specific post following the REST API (retrieve, /posts/id:, GET)

	fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
	.then((response) => response.json())
	.then((json) => console.log(json));